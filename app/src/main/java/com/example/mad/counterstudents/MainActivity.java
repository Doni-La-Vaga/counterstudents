package com.example.mad.counterstudents;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;
import android.view.View;

import java.util.Locale;

/**
 * Класс {@code MainActivity} предназначен для работы простого счётчика
 *
 * @author Визирякин Павел группа 14ИТ18К
 */
public class MainActivity extends AppCompatActivity {

    // Начальное состояние счётчика
    private Integer countOfStudents = 0;

    /**
     * Вызывается при создании Activity
     *
     * @param savedInstanceState - восстановление временных данных при изменении Активности.
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    /**
     * Инкрементирует счётчик при нажатии на кнопку "ДОБАВИТЬ СТУДЕНТА"
     *
     * @param view - вьюшка
     */
    public void onClickButtonAddStudents(View view){
        countOfStudents++;
        outputCountOfStudent();
    }

    /**
     * Сохраняет состояние Activity
     *
     * @param outState - временные данные в процессе работы Activity
     */
    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt("count", countOfStudents);
    }

    /**
     * Восстанавливает состояние приложения после изменений Activity
     *
     * @param savedInstanceState - данные для восстановления приложения
     */
    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        if (savedInstanceState != null &&
                savedInstanceState.containsKey("count")) {
            countOfStudents = savedInstanceState.getInt("count");
            outputCountOfStudent();
        }
    }


    /**
     * Преобразует {@code countOfStudents} в строку и выводит её на экран
     */
    protected void outputCountOfStudent(){
        TextView counterView = (TextView)findViewById(R.id.txt_counter);
        counterView.setText(String.format(Locale.getDefault(), "%d", countOfStudents));
    }
}
